﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Update_wallet_api_script : MonoBehaviour
{
    private JSONNode jsonResult;
    string rawJson;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnApplicationQuit()
    {

        upload_data();
    }

    public void upload_data()
    {
        StartCoroutine("Upload"); 
    }

    IEnumerator Upload()
    {
        //Debug.Log("device_token" + device_token);
        WWWForm form = new WWWForm();
        //form.AddField("login_type", "guest");

        form.AddField("type", "Gold");
        form.AddField("wallet_coin ", GetComponent<Redeem_api_script>().cm._Gold_coin_current_total);
        //form.AddField("coin", coins);



        UnityWebRequest www = UnityWebRequest.Post("http://134.209.103.120/CoinPusher/mapi/1/update-wallet", form);
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        yield return www.SendWebRequest();
        Debug.Log("www: " + www.responseCode);
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            Check_Internet_connection();
        }
        else
        {
            StartCoroutine("Upload1");
            //close_Retry_panel();
            if (Notice_panel_script.Instance != null)
            {
                close_Retry_panel();
            }
            rawJson = www.downloadHandler.text;
            //Get_response_data();
            Debug.Log("Form upload complete!");
        }

    }
    IEnumerator Upload1()
    {
        //Debug.Log("device_token" + device_token);
        WWWForm form = new WWWForm();
        //form.AddField("login_type", "guest");

        form.AddField("type", "Green");
        form.AddField("wallet_coin ", GetComponent<Redeem_api_script>().cm._Green_coin_current_total.ToString());
        //form.AddField("coin", coins);



        UnityWebRequest www = UnityWebRequest.Post("http://134.209.103.120/CoinPusher/mapi/1/update-wallet", form);
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        yield return www.SendWebRequest();
        Debug.Log("www: " + www.responseCode);
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            //Check_Internet_connection();
        }
        else
        {
            StartCoroutine("Upload2");
            //close_Retry_panel();
            rawJson = www.downloadHandler.text;
            //Get_response_data();
            Debug.Log("Form upload complete!");
        }

    }
    IEnumerator Upload2()
    {
        //Debug.Log("device_token" + device_token);
        WWWForm form = new WWWForm();
        //form.AddField("login_type", "guest");

        form.AddField("type", "Black Bar");
        form.AddField("wallet_coin ", GetComponent<Redeem_api_script>().cm._Black_coin_current_total);
        //form.AddField("coin", coins);



        UnityWebRequest www = UnityWebRequest.Post("http://134.209.103.120/CoinPusher/mapi/1/update-wallet", form);
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        yield return www.SendWebRequest();
        Debug.Log("www: " + www.responseCode);
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            //Check_Internet_connection();
        }
        else
        {
            //StartCoroutine("Upload2");
            //close_Retry_panel();
            rawJson = www.downloadHandler.text;
            //Get_response_data();
            Debug.Log("Form upload complete!");
        }

    }
    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_panel.SetActive(true);
                    Notice_panel_script.Instance.Something_went_wrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_panel.SetActive(true);
                    Notice_panel_script.Instance.no_internet_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.no_internet_panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }

    public void Retry_connection()
    {
        upload_data();

        Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(false);
        Notice_panel_script.Instance.no_internet_panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.notice_panel.SetActive(false);
        upload_data();
    }
    public void close_Retry_panel()
    {
        Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(false);
        Notice_panel_script.Instance.no_internet_panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.notice_panel.SetActive(false);
    }
}
