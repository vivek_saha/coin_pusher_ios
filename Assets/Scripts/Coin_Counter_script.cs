﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CoinPusher;

public enum Coin_type { gold, green, black , shake };
public enum Coin__size_type { small, big };
public class Coin_Counter_script : MonoBehaviour
{
    //public static Coin_Counter_script Instance;
    public Toys_counter_shop_panel_script toys_shop_script;
    Coin_manager c_manage;
    //public GameObject Floating_text;

    [Header("not decreasing counter:")]
    int Coin_counter;
    int Green_coin_counter;
    int Shake_coin_counter;
    int Big_coin_counter;
    int Big_Green_coin_counter;
    int Black_bar_coin_counter;

    [Header("Toys:")]
    int Orange_counter;
    int Carrot_counter;
    int Apple_counter;
    int Lemon_counter;
    int Banana_counter;
    int Bell_counter;
    int Lolipop_counter;
    int Candy_counter;

    //private void Awake()
    //{
    //    Instance = this;
    //}

    // Start is called before the first frame update
    void Start()
    {
        c_manage = GetComponent<Coin_manager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision != null)
        {
            if(AudioManager.Instance.Vibrate_bool)
            {
                //AudioManager.Vibrate(100);
                //Vibration.Vibrate(20);
                Vibration.VibratePop();
                //Vibration.VibratePop();
                //Handheld.Vibrate();
            }
            if (AudioManager.Instance.Sound_bool)
            {
                AudioManager.Instance.coin_fall_sounds();
            }
        }
        if (collision.gameObject.tag == "Coin")
        {
            Coin_counter++;
            c_manage.Get_counter_coin(Coin_type.gold, Coin__size_type.small,collision.transform);
        }
        if (collision.gameObject.tag == "Green_coin")
        {
            Green_coin_counter++;
            c_manage.Get_counter_coin(Coin_type.green, Coin__size_type.small, collision.transform);
        }
        if (collision.gameObject.tag == "Shake_coin")
        {
            Shake_coin_counter++;
            c_manage.Get_counter_coin(Coin_type.shake, Coin__size_type.big, collision.transform);
        }
        if (collision.gameObject.tag == "Big_coin")
        {
            Big_coin_counter++;
            c_manage.Get_counter_coin(Coin_type.gold, Coin__size_type.big, collision.transform);
        }
        if (collision.gameObject.tag == "Big_Green_coin")
        {
            Big_Green_coin_counter++;
            c_manage.Get_counter_coin(Coin_type.green, Coin__size_type.big, collision.transform);
        }
        if (collision.gameObject.tag == "Black_bar_coin")
        {
            Black_bar_coin_counter++;
            c_manage.Get_counter_coin(Coin_type.black, Coin__size_type.big, collision.transform);
        }
        if (collision.gameObject.tag == "Banana")
        {
            toys_shop_script.banana_counter++;
        }
        if (collision.gameObject.tag == "Apple")
        {
            toys_shop_script.apple_counter++;
        }
        if (collision.gameObject.tag == "Orange")
        {
            toys_shop_script.orange_counter++;
        }
        if (collision.gameObject.tag == "Lemon")
        {
            toys_shop_script.lemon_counter++;
        }
        if (collision.gameObject.tag == "Carrot")
        {
            toys_shop_script.carrot_counter++;
        }
        if (collision.gameObject.tag == "Lolipop")
        {
            toys_shop_script.lolipop_counter++;
        }
        if (collision.gameObject.tag == "Bell")
        {
            toys_shop_script.bell_counter++;
        }
        if (collision.gameObject.tag == "Candy")
        {
            toys_shop_script.candy_counter++;
        }
        
        Destroy(collision.gameObject);
    }
}
