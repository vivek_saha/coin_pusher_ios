﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamemanager : MonoBehaviour
{
    public static Gamemanager Instance;
    public GameObject nem_popup;
    public RectTransform origin;
    public Transform canvas_content;
    public Setting_panel_script set_script;
    public int App_ver_code;
    public GameObject monster_hit_spark;
    int ads_counter = 0;
    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        //set_script.set_toogles();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void nem_popup_fun(string msg)
    {
        GameObject a = Instantiate(nem_popup);
        a.GetComponent<nem_popup_script>().msg.text = msg;
        a.transform.SetParent(canvas_content);
        a.transform.position = origin.transform.position;
    }

    public void stop_time()
    {

        if(ads_counter>= Ads_initialize_API.Instance.ads_click)
        {
            Ads_priority_script.Instance.Show_interrestial();
            ads_counter = 0;
        }
        else
        {
            ads_counter++;
        }
        Time.timeScale = 0;
    }

    public void start_time()
    {
        Time.timeScale = 1;
    }
}
