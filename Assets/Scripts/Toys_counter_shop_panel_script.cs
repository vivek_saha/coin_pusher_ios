﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Toys_counter_shop_panel_script : MonoBehaviour
{

    public TextMeshProUGUI[] Current_counter;
    public TextMeshProUGUI[] Total_counter;
    public int banana_counter
    {
        get
        {
            return PlayerPrefs.GetInt("banana_counter", 0);
        }
        set
        {
            PlayerPrefs.SetInt("banana_counter", value);
        }
    }
    public int carrot_counter
    {
        get
        {
            return PlayerPrefs.GetInt("carrot_counter", 0);
        }
        set
        {
            PlayerPrefs.SetInt("carrot_counter", value);
        }
    }
    public int apple_counter
    {
        get
        {
            return PlayerPrefs.GetInt("apple_counter", 0);
        }
        set
        {
            PlayerPrefs.SetInt("apple_counter", value);
        }
    }
    public int lemon_counter
    {
        get
        {
            return PlayerPrefs.GetInt("lemon_counter", 0);
        }
        set
        {
            PlayerPrefs.SetInt("lemon_counter", value);
        }
    }
    public int orange_counter
    {
        get
        {
            return PlayerPrefs.GetInt("orange_counter", 0);
        }
        set
        {
            PlayerPrefs.SetInt("orange_counter", value);
        }
    }
    public int bell_counter
    {
        get
        {
            return PlayerPrefs.GetInt("bell_counter", 0);
        }
        set
        {
            PlayerPrefs.SetInt("bell_counter", value);
        }
    }
    public int candy_counter
    {
        get
        {
            return PlayerPrefs.GetInt("candy_counter", 0);
        }
        set
        {
            PlayerPrefs.SetInt("candy_counter", value);
        }
    }
    public int lolipop_counter
    {
        get
        {
            return PlayerPrefs.GetInt("lolipop_counter", 0);
        }
        set
        {
            PlayerPrefs.SetInt("lolipop_counter", value);
        }
    }


    void Start()
    {
        set_total_counter();
    }

    private void OnEnable()
    {
        set_current_counter();   
    }
    public void set_total_counter()
    {
        int a = 15;
        for(int i=0;i<8;i++)
        {
            
            Total_counter[i].text = (a).ToString();
            a += 3;
        }
    }
    public void set_current_counter()
    {
        Current_counter[0].text = banana_counter.ToString();
        Current_counter[1].text = carrot_counter.ToString();
        Current_counter[2].text = apple_counter.ToString();
        Current_counter[3].text = lemon_counter.ToString();
        Current_counter[4].text = orange_counter.ToString();
        Current_counter[5].text = bell_counter.ToString();
        Current_counter[6].text = candy_counter.ToString();
        Current_counter[7].text = lolipop_counter.ToString();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
