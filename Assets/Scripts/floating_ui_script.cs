﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class floating_ui_script : MonoBehaviour
{
    public Image coin_icon;
    public TextMeshProUGUI _text;
    //public enum Coin_type { gold, green, black };
    public Coin_type _coin_type;
    public Sprite[] _coin_sprite;
    public Color[] text_color;
    public RectTransform main_location_rect;
    RectTransform _rect;


    public void set_values(Coin_type cd, string _value)
    {
        _coin_type = cd;

        switch (_coin_type)
        {
            case Coin_type.gold:
                _text.text = "+" + _value;
                _text.color = text_color[0];
                coin_icon.sprite = _coin_sprite[0];
                break;
            case Coin_type.green:
                _text.text = "+" + _value;
                _text.color = text_color[1];
                coin_icon.sprite = _coin_sprite[1];
                break;
            case Coin_type.black:
                _text.text = "+" + _value;
                _text.color = text_color[2];
                coin_icon.sprite = _coin_sprite[2];
                break;
           
        }

    }
   
    
    // Start is called before the first frame update
    void Start()
    {
        _rect = GetComponent<RectTransform>();
        Move_up();
    }


    public void Move_up()
    {
        transform.DOLocalMoveY(transform.localPosition.y + 300, 1f).OnComplete(() => chnage_coin());
    }


    public void chnage_coin()
    {
        _text.gameObject.SetActive(false);
        coin_icon.gameObject.SetActive(true);
        //Move_coin_to_to_parent();
        Invoke("Move_coin_to_to_parent", 0.2f);
    }
    public void Move_coin_to_to_parent()
    {
        //_rect.DOMove(main_location[0].transform.position, 0.5f);
        //target_rect = main_location[0].GetComponent<RectTransform>();
        //Debug.Log(target_rect.position);
        _rect.DOMove(main_location_rect.position, 0.5f).OnComplete(() => add_coin());

    }

    public void add_coin()
    {
        //addcoin

        Platform_Manager.Instance.Add_coin_to_ui(_coin_type, _text.text);
        Destroy(this.gameObject);

    }
    // Update is called once per frame
    void Update()
    {

    }
}