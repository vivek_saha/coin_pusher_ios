﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Model_skin_chnager_script : MonoBehaviour
{

    public Model_skin_type skin_type;
    [HideInInspector]
    public GameObject Normal_top_part, Normal_bottom_part, Normal_poll, Normal_reel;
    [HideInInspector]
    public GameObject circus_top_part, circus_bottom_part, circus_poll, circus_reel;
    public GameObject bg, bg_cloud;
    public Sprite normal_bg, circus_bg;

    // Start is called before the first frame update
    void Start()
    {
        chnage_thme_models();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void chnage_thme_models()
    {
        if (skin_type == Model_skin_type.normal)
        {
            Normal_top_part.SetActive(true);
            circus_top_part.SetActive(false);
            Normal_bottom_part.GetComponent<MeshRenderer>().enabled = true;
            circus_bottom_part.SetActive(false);
            Normal_poll.GetComponent<MeshRenderer>().enabled = true;
            circus_poll.SetActive(false);
            Normal_reel.GetComponent<MeshRenderer>().enabled = true;
            circus_reel.SetActive(false);
            bg.GetComponent<SpriteRenderer>().sprite = normal_bg;
            bg_cloud.GetComponent<MeshRenderer>().material.color = Color.white;
        }
        else
        {
            Normal_top_part.SetActive(false);
            circus_top_part.SetActive(true);
            Normal_bottom_part.GetComponent<MeshRenderer>().enabled = false;
            circus_bottom_part.SetActive(true);
            Normal_poll.GetComponent<MeshRenderer>().enabled = false;
            circus_poll.SetActive(true);
            Normal_reel.GetComponent<MeshRenderer>().enabled = false;
            circus_reel.SetActive(true);
            bg.GetComponent<SpriteRenderer>().sprite = circus_bg;
            Color color;
            if (ColorUtility.TryParseHtmlString("#8424A4", out color))
            { bg_cloud.GetComponent<MeshRenderer>().material.color = color; }
            
        }
    }
}
