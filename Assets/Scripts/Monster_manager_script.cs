﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster_manager_script : MonoBehaviour
{

    public GameObject monster, loader;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Disable_monster()
    {
        //Platform_Manager.Instance.Spawan_reward();
        Platform_Manager.Instance.Add_boost_mode_exp();
        reward();
        monster.SetActive(false);
        loader.SetActive(false);
        StartCoroutine("respawn_monster");
    }

    IEnumerator respawn_monster()
    {
        yield return new WaitForSeconds(5f);
        enable_monster();
    }

    public void enable_monster()
    {
        monster.GetComponent<Moster_main_script>().Manual_Start();
        
        monster.SetActive(true);
        loader.SetActive(true);
    }

    void reward()
    {
        //Platform_Manager.Instance.Spawan_reward(loader.GetComponent<Loader_monster_script>().reward_amount);

        switch (loader.GetComponent<Loader_monster_script>().reward_amount)
        {

            case 3:
                Platform_Manager.Instance.Spawan_reward(0);
                break;
            case 5:
                Platform_Manager.Instance.Spawan_reward(1);
                break;
            case 10:
                Platform_Manager.Instance.Spawan_reward(2);
                break;
            default:
                break;
        }
    }
}
